//
//  TPEditViewController.h
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"

@interface TPEditViewController : UIViewController
{
    GADBannerView *bannerView_;

    IBOutlet UIButton *btnForCancel;
    
    IBOutlet UIButton *btnForSticker;
    IBOutlet UIButton *btnForSave;
    UIImage *tt;
    BOOL is_erase;

    BOOL is_hori;
    
    IBOutlet UIView *viewForTool;
    
    NSInteger sticker_count;
    
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imageViewForEdit;
@property (strong, nonatomic) UIImage *editImage;

- (IBAction)cancelAction:(id)sender;

- (IBAction)saveAction:(id)sender;
- (IBAction)finishAction:(id)sender;
- (IBAction)selectStickerAction:(id)sender;

- (IBAction)toolBarAction:(id)sender;



@end
