//
//  TPShareViewController.m
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPShareViewController.h"
#import "CMPopTipView.h"
#import <Chartboost/Chartboost.h>

@interface TPShareViewController ()<CMPopTipViewDelegate>
{
    CMPopTipView *currentPopView;

}
@end

@implementation TPShareViewController
@synthesize imageForShare,imageViewForShare,fromCamera,dic;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _viewForActionSheet.hidden = YES;
    
    CGRect rect = _viewForActionSheet.frame;
    
    rect.origin.y = self.view.frame.size.height + 10;
    
    
    [_viewForActionSheet setFrame:rect];
//    [_viewForActionSheet setFrame:CGRectMake(0, self.view.frame.size.height + 10, self.view.frame.size.width, self.view.frame.size.width / 2)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
  
//    [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(showCB:) userInfo:nil repeats:YES];
    
}

- (void)showCB:(NSTimer *)timer
{
   // [[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
     [Chartboost showInterstitial:CBLocationHomeScreen];
    
}

- (void)showAds
{
   #ifdef FREE_VERSION
    CGPoint origin = CGPointMake(0.0f,0.0f);
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait origin:origin];
    
    // Specify the ad unit ID.
    NSString *admobID;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        admobID = @"a153c86b0820e99";
        
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        admobID = @"a153c86b3090ecc";
    }
    bannerView_.adUnitID = admobID;//;
    
    
    bannerView_.rootViewController = self;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    //    if (![ud objectForKey:PURCHASED]) {
    //
    //        [self.view addSubview:bannerView_];
    //    }
    //NSString
    
    [self.view addSubview:bannerView_];
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    [ALInterstitialAd showOver:[[UIApplication sharedApplication] keyWindow]];
    
    
    //[[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
     [Chartboost showInterstitial:CBLocationHomeScreen];

#endif
    
}


- (void)loadView
{
    [super loadView];
    [self initImage];
}

- (void)initImage
{
    
    NSLog(@"%f",imageForShare.size.width);
    
    [self resetImageViewSize];
}

- (void)resetImageViewSize
{
//    float ratio = imageForShare.size.width / imageForShare.size.height;
//    
//    rect.size.width = self.view.frame.size.width;
//    rect.size.height = self.view.frame.size.width / ratio;
//    
//    float _y = (self.view.frame.size.height - rect.size.height) / 2;
//    rect.origin.y = _y;
//

    CGRect rect = imageViewForShare.frame;

    float width = self.view.frame.size.width;
    float height = self.view.frame.size.width;
    if (imageForShare.size.width < imageForShare.size.height) {
        width = imageForShare.size.width / imageForShare.size.height * self.view.frame.size.width;
    }
    else
    {
        height = imageForShare.size.height * width / imageForShare.size.width;
    }
    
    rect.size.width = width;
    rect.size.height = height;
    
    [imageViewForShare setFrame:rect];
     imageViewForShare.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [imageViewForShare setImage:imageForShare];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showShareActionSheet
{
    [UIView animateWithDuration:0.5f delay:0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect shareRect = _viewForActionSheet.frame;
        [_viewForActionSheet setHidden:NO];
        shareRect.origin.y = self.view.frame.size.height - _viewForActionSheet.frame.size.height;
        [_viewForActionSheet setFrame:shareRect];
    } completion:nil];
    
}

- (void)hideShareActionSheet
{
    [UIView animateWithDuration:0.5f delay:0.1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect shareRect = _viewForActionSheet.frame;
        shareRect.origin.y = self.view.frame.size.height + 10;
        [_viewForActionSheet setFrame:shareRect];
    } completion:^(BOOL finished) {
        [_viewForActionSheet setHidden:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)helpAction:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    if (currentPopView != nil) {
        [currentPopView dismissAnimated:YES];
        currentPopView = nil;
    }
    else
    {
        CMPopTipView *popTipView  = [[CMPopTipView alloc] initWithMessage:@"- TO SET IMAGE AS PROFILE PIC:\n- Save Image to Camera Roll\n- Tinder requires Facebook for most Tinder profile information including Profile pic. You will need to export desired profile picture to Facebook and then choose your Tinder Profile pic."];
        popTipView.textColor = [UIColor whiteColor];
        popTipView.delegate = self;
        popTipView.backgroundColor = [UIColor redColor];
        popTipView.animation = arc4random() % 2;
        currentPopView = popTipView;
        [currentPopView presentPointingAtView:button inView:self.view animated:YES];
        
    }

}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView {
    //	[visiblePopTipViews removeObject:popTipView];
	currentPopView = nil;
}


- (IBAction)newAction:(id)sender {
    
    
    if (fromCamera) {
        [self.navigationController popToRootViewControllerAnimated:YES];

    }
    else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)saveAction:(id)sender {
    
    [self showShareActionSheet];
    
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

    
//    if (fromCamera) {
//        
//    }
//    else
//    {
//        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//    }

    
}

- (IBAction)saveToAlbumAction:(id)sender {
    
    UIImage *_image = imageForShare;
    
    UIImageWriteToSavedPhotosAlbum(_image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        [[[UIAlertView alloc] initWithTitle:@"Error!" message:@"Saving Error." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }else
    {
        [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Successfully saved photo." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
    }
}

- (IBAction)shareActions:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    
    switch (button.tag) {
        case 100:
        {
            [self shareToTwitter];
        }
            break;
        case 101:
            [self shareToInstagram];
            break;
        case 102:
            [self shareToEmail];
            break;
        case 103:
            [self shareToFaceBook];
            break;
        default:
            break;
    }
}

- (IBAction)cancelAction:(id)sender {
    [self hideShareActionSheet];
}

- (void)shareToTwitter
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"Created  by TigerPrank App!"];
        [tweetSheet addURL:[NSURL URLWithString:@""]];
        [tweetSheet addImage:imageForShare];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:@"Confirm your twitter setting."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)shareToInstagram
{
    UIImage * screenshot = imageForShare;
    
    // UIImage *screenshot = [UIImage imageNamed:@"splash@2x.png"];
    NSString *savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Screenshot.igo"];
    
    // Write image to PNG
    [UIImageJPEGRepresentation(screenshot, 1.0) writeToFile:savePath atomically:YES];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        //imageToUpload is a file path with .ig file extension
        dic = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
        dic.UTI = @"com.instagram.exclusivegram";
        dic.delegate = self;
        
        dic.annotation = [NSDictionary dictionaryWithObject:@"Uploaded using #TigerPrank App" forKey:@"InstagramCaption"];
        [dic presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
        
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Please install Instagram." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ]show];
    }

}

- (void)shareToEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailView = [[MFMailComposeViewController alloc] init];
        mailView.mailComposeDelegate = self;
        [mailView setSubject:@"Wonderfull!!!"];
        [mailView setMessageBody:@"Created by TigerPrank App!" isHTML:YES];
        NSData *attachmentData = UIImageJPEGRepresentation(imageForShare, 1.0);
        [mailView addAttachmentData:attachmentData mimeType:@"image/jpeg" fileName:@"image.jpg"];
        [self presentViewController:mailView animated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Please register your email address into your device." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
    }

}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"error" message:[NSString stringWithFormat:@"error %@",[error description]] delegate:nil cancelButtonTitle:@"dismiss" otherButtonTitles: nil];
        [alert show];
    }/* else {
      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Mail transfered successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [alert show];
      [self dismissViewControllerAnimated:YES completion:nil];
      } */
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)shareToFaceBook
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:@"Created by TigerPrank App!"];
        [controller addURL:[NSURL URLWithString:@""]];
        [controller addImage:imageForShare];
        
        [self presentViewController:controller animated:YES completion:Nil];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:@"Confirm your facebook setting."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }

}

@end
