//
//  TPGlobal.h
//  TigerPrank
//
//  Created by ellisa on 7/13/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPGlobal : NSObject
+ (UIImage *)flipImageVertically:(UIImage *)originalImage;
+ (UIImage *)flipImageHorizontally:(UIImage *)originalImage;

+ (BOOL)isPurchased;
+ (void)setPurchased;
@end
