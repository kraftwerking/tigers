//
//  TPShareViewController.h
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "GADBannerView.h"

@interface TPShareViewController : UIViewController<MFMailComposeViewControllerDelegate,UIDocumentInteractionControllerDelegate>
{
    
    GADBannerView *bannerView_;

    
}



@property (strong, nonatomic) IBOutlet UIImageView *imageViewForShare;
@property (strong, nonatomic) UIImage *imageForShare;

@property (nonatomic) BOOL  fromCamera;
@property (nonatomic, retain) UIDocumentInteractionController *dic; 
@property (strong, nonatomic) IBOutlet UIView *viewForActionSheet;


- (IBAction)helpAction:(id)sender;


- (IBAction)newAction:(id)sender;

- (IBAction)saveAction:(id)sender;

- (IBAction)backAction:(id)sender;

- (IBAction)saveToAlbumAction:(id)sender;

- (IBAction)shareActions:(id)sender;


- (IBAction)cancelAction:(id)sender;


@end
