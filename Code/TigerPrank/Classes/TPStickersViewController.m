//
//  TPStickersViewController.m
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPStickersViewController.h"
#import "TPMoreStickerViewController.h"
#import "TPGlobal.h"

@interface TPStickersViewController ()<TPMoreStickerViewControllerDelegate>

@end

@implementation TPStickersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"MyCell"];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Navigation bar
    
    self.title = @"Choose Sticker";
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
//    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"topBar.png"] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    UIButton *btnForMore = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45 , 25)];
    [btnForMore setImage:[UIImage imageNamed:@"moreSticker_ipad"] forState:UIControlStateNormal];
//    [btnForMore setTitle:@"WantMore?" forState:UIControlStateNormal];
//    [btnForMore setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnForMore addTarget:self action:@selector(moreTiger:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnForMore];
    UIBarButtonItem *negativeRightSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [negativeRightSpacer setWidth:-(self.view.frame.size.width / 40)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeRightSpacer,rightBarItem, nil];
    if ([TPGlobal isPurchased]) {
        btnForMore.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)moreTiger:(UIButton *)sender
{
    TPMoreStickerViewController *moreVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreVC"];
    moreVC.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:moreVC];
    
    
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

#pragma mark -
#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([TPGlobal isPurchased]) {
        return 87;
    }
    
    return 20;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCell" forIndexPath:indexPath];
    UIImageView *cellImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, myCell.frame.size.width, myCell.frame.size.height)];
    if (indexPath.row < 20) {
        [cellImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"s%d",indexPath.row + 1]]];

    }
    else
    {
        [cellImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d",indexPath.row - 19]]];

    }
    
    
    [myCell addSubview:cellImgView];
    
    cellImgView.backgroundColor = [UIColor whiteColor];
    return myCell;
}
#pragma mark UICollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *image;
    
    if (indexPath.row < 20) {
        image = [UIImage imageNamed:[NSString stringWithFormat:@"s%d",indexPath.row + 1]];
    }
    else
    {
        image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",indexPath.row - 19]];
    }
    
    if ([self.delegate respondsToSelector:@selector(imageForSticker:didFinished:)]) {
        [self.delegate imageForSticker:self didFinished:image];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UICollectionViewDelegateFlow Layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float w = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        w = 106.0f;
    }
    else
    {
        w = (self.view.frame.size.width - 6) / 3;
    }
    
    CGSize retVal = CGSizeMake(w, w);
    return retVal;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)purchasedForSticker:(TPMoreStickerViewController *)stickerViewController didFinished:(BOOL)finished
{
    [stickerViewController.navigationController dismissViewControllerAnimated:YES completion:^{
        [myCollectionView reloadData];
    }];
}

- (void)purchaseDidCancel:(TPMoreStickerViewController *)stickerViewController{
    [stickerViewController.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
