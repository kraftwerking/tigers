//
//  TPMoreStickerViewController.m
//  TigerPrank
//
//  Created by ellisa on 7/17/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPMoreStickerViewController.h"
#import "TPGlobal.h"
#import "MBProgressHUD.h"
@interface TPMoreStickerViewController ()<UIAlertViewDelegate>
@property (nonatomic, strong) NSArray *products;
@end

@implementation TPMoreStickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isPurchased = NO;
    
    [myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"MyCell"];

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    self.title = @"More Stickers";
    [self reload];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productFailed:) name:IAPHelperProductPurchaseFailedNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reload
{
    self.products = nil;
    
    if ([TPGlobal isPurchased]) {
        
    }
    else
    {
        [[MyIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                self.products = products;
            }
        }];

    }
}

- (void)productFailed:(NSNotification *)notification
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)productPurchased:(NSNotification *)notification
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    NSString *productIdentifier = notification.object;
    
    
    [self.products enumerateObjectsUsingBlock:^(SKProduct *product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            
            *stop = YES;
            [TPGlobal setPurchased];
            
            if ([self.delegate respondsToSelector:@selector(purchasedForSticker:didFinished:)]) {
                 [self.delegate purchasedForSticker:self didFinished:YES];
            }
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actions:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    switch (button.tag) {
        case 10:
        {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
            break;
        case 11:
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            [[MyIAPHelper sharedInstance] restoreCompletedTransactions];
        }
            break;
        case 12:
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            SKProduct *product = [self.products objectAtIndex:0];
            
            [[MyIAPHelper sharedInstance] buyProduct:product];
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 67;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCell" forIndexPath:indexPath];
    UIImageView *cellImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, myCell.frame.size.width, myCell.frame.size.height)];
    [cellImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d",indexPath.row + 1]]];
    
    [myCell addSubview:cellImgView];
    
    cellImgView.backgroundColor = [UIColor whiteColor];
    return myCell;
}
#pragma mark UICollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[[UIAlertView alloc] initWithTitle:nil message:@"Would you like to get more tigers?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil]show];
//    if ([self.delegate respondsToSelector:@selector(purchasedForSticker:didFinished:)]) {
//        [self.delegate purchasedForSticker:self didFinished:YES];
//    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UICollectionViewDelegateFlow Layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float w = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        w = 106.0f;
    }
    else
    {
        w = (self.view.frame.size.width - 6) / 3;
    }
    
    CGSize retVal = CGSizeMake(w, w);
    return retVal;
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            SKProduct *product = [self.products objectAtIndex:0];
            
            [[MyIAPHelper sharedInstance] buyProduct:product];
        }
            break;
        case 1:
        {
            
        }
            break;
        default:
            break;
    }
}

@end
