//
//  TPEditViewController.m
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPEditViewController.h"
#import "TPShareViewController.h"
#import "TPStickersViewController.h"
#import "TPSticerView.h"
#import "CMPopTipView.h"
#import "TPGlobal.h"
#define TOPTAG  100
@interface TPEditViewController ()<TPStickersViewControllerDelegate, TPStickerViewDelegate,CMPopTipViewDelegate>
{
    
    UIView *workingView;
    TPSticerView *localSticker;
    TPSticerView * m_pActiveStickerView;
    CMPopTipView *currentPopView;
    NSMutableArray *arrForSticker;
    
}
@end

@implementation TPEditViewController
@synthesize editImage,imageViewForEdit;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForSticker = [NSMutableArray array];
    is_erase = NO;
    is_hori = NO;
    
    sticker_count = 0;
    [self showAds];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
     
//    [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(showCB:) userInfo:nil repeats:NO];
    [self resizeImageView];

    
}

- (void)showAds
{
   #ifdef FREE_VERSION
    CGPoint origin = CGPointMake(0.0f,0.0f);
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait origin:origin];
    
    // Specify the ad unit ID.
    NSString *admobID;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        admobID = @"a153c86b0820e99";
        
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        admobID = @"a153c86b3090ecc";
    }
    bannerView_.adUnitID = admobID;//;
    
    
    bannerView_.rootViewController = self;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    //    if (![ud objectForKey:PURCHASED]) {
    //
    //        [self.view addSubview:bannerView_];
    //    }
    
    [self.view addSubview:bannerView_];
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    
    [ALInterstitialAd showOver:[[UIApplication sharedApplication] keyWindow]];
    
    
    //[[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
    [Chartboost showInterstitial:CBLocationHomeScreen];

#endif

}

- (void)showCB:(NSTimer *)timer
{
    //[[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
    [Chartboost showInterstitial:CBLocationHomeScreen];

}

- (void)resizeImageView
{
self.navigationController.navigationBarHidden = YES;
    CGRect rect = imageViewForEdit.frame;
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.width;
    if (editImage.size.width < editImage.size.height) {
        width = editImage.size.width / editImage.size.height * self.view.frame.size.width;
    }
    else
    {
        height = editImage.size.height * width / editImage.size.width;
    }
    
    rect.size.width = width;
    rect.size.height = height;
    
    [imageViewForEdit setFrame:rect];
    
    imageViewForEdit.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);

    [imageViewForEdit setImage:editImage];
    
    NSLog(@"%f, -%f", editImage.size.width, editImage.size.height);
    NSLog(@"%f, --%f",imageViewForEdit.frame.size.width, imageViewForEdit.frame.size.height);
    [imageViewForEdit setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setActiveSticker:)];
    [imageViewForEdit addGestureRecognizer:gesture];
    
    workingView = [[UIView alloc] initWithFrame:[self.view convertRect:self.imageViewForEdit.frame fromView:self.imageViewForEdit.superview]];
    [workingView setClipsToBounds:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITapGesture Action
- (void)setActiveSticker:(UITapGestureRecognizer *)_gesture
{
    [TPSticerView setActiveStickerView:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelAction:(id)sender {
    
 
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender {
    
    [TPSticerView setActiveStickerView:nil];
    
    
    UIImage *image = imageViewForEdit.image;
    UIGraphicsBeginImageContext(image.size);
    [image drawAtPoint:CGPointZero];
    
    CGFloat scale = image.size.width / imageViewForEdit.frame.size.width;
    
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
    
    [imageViewForEdit.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    tt = tmp;

    TPShareViewController *tpShareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareVC"];
    
    tpShareVC.imageForShare = tt;
    tpShareVC.fromCamera = NO;
    [self.navigationController pushViewController:tpShareVC animated:YES];
    
    
    
}

- (IBAction)finishAction:(id)sender {
    
}

- (IBAction)selectStickerAction:(id)sender {
    
    TPStickersViewController *tpStickerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StickerVC"];
    tpStickerVC.delegate = self;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:tpStickerVC];
    
    [self.navigationController presentViewController:nav animated:YES completion:nil];

}

- (IBAction)toolBarAction:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    switch (button.tag) {
        case TOPTAG:
        {
//            TPSticerView *_view = (TPSticerView *)[imageViewForEdit viewWithTag:1000];
            if (m_pActiveStickerView != nil) {
                if (m_pActiveStickerView.m_bIsEditing == NO)
                    [m_pActiveStickerView doChangetoEditingState];
            }
//            _view.is_enable = NO;
//            is_erase = YES;
        }
            break;
            
        case TOPTAG + 1:
        {
            
            
            [m_pActiveStickerView removeActiveSticker];
            sticker_count--;
            if (sticker_count == 0) {
                viewForTool.hidden = YES;
            }
            
        }
            break;
            
        case TOPTAG + 2:
        {
            UIImage * image;
            image = m_pActiveStickerView.imageView.image;
            
            [TPSticerView setConvertedImage:m_pActiveStickerView image:[TPGlobal flipImageVertically:image]];
            image = nil;

            if (m_pActiveStickerView.m_bIsEditing == YES)
                [m_pActiveStickerView doChangetoEditingState];

        }
            break;
            
        case TOPTAG + 3:
        {
            UIImage * image;
            image = m_pActiveStickerView.imageView.image;
            
            [TPSticerView setConvertedImage:m_pActiveStickerView image:[TPGlobal flipImageHorizontally:image]];
            image = nil;
            
            if (m_pActiveStickerView.m_bIsEditing == YES)
                [m_pActiveStickerView doChangetoEditingState];

        }
            break;
        case TOPTAG + 4:
        {
            [m_pActiveStickerView changeActiveSticker];
        }
            break;
        case TOPTAG + 5:
        {
            [m_pActiveStickerView changeActiveSticker];
        }
            break;
        case TOPTAG + 6:
        {
            [TPSticerView setActiveStickerView:nil];
            
//            [[[UIAlertView alloc] initWithTitle:nil message:@"- TO SET IMAGE AS PROFILE PIC:\n - Save Image to Camera Roll \n - Tinder requires Facebook for most Tinder profile information including Profile pic. You will need to export desired profile picture to Facebook and then choose your Tinder Profile pic." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        }
            break;
        case TOPTAG + 7:
        {
                        
            
        }
            break;
        default:
            if (m_pActiveStickerView.m_bIsEditing == YES)
                [m_pActiveStickerView doChangetoEditingState];
            break;
    }
}


- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView {
//	[visiblePopTipViews removeObject:popTipView];
	currentPopView = nil;
}


#pragma mark TPStickersViewControllerDelegate method

- (void)stickerDidCancel:(TPStickersViewController *)stickerViewController
{
    [stickerViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageForSticker:(TPStickersViewController *)stickerViewController didFinished:(UIImage *)stickerImage
{
    [stickerViewController dismissViewControllerAnimated:YES completion:^{
        
        TPSticerView *view = [[TPSticerView alloc] initWithImage:stickerImage];
        view.is_enable = YES;
        CGFloat ratio = MIN((0.5 * imageViewForEdit.frame.size.width) / view.frame.size.width, (0.5 * imageViewForEdit.frame.size.height) / view.frame.size.height);
        
        [view setScale:ratio];
        view.center = CGPointMake(imageViewForEdit.frame.size.width / 2, imageViewForEdit.frame.size.height / 2);
        [imageViewForEdit addSubview:view];
        
        m_pActiveStickerView = view;
        view.delegate = self;
        [TPSticerView setActiveStickerView:view];
        sticker_count ++;

        NSLog(@"%d",view.tag);
        view.alpha = 0.2;
        [UIView animateWithDuration:0.3f animations:^{
            view.alpha = 1.0f;
        }];
//        localSticker = view;
//        [arrForSticker addObject:localSticker];
        
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    TPSticerView *__view = (TPSticerView *)m_pActiveStickerView;
    
    if ([touch view] == __view.imageView) {
            is_erase = YES;
        }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (is_erase) {
        TPSticerView *__view = (TPSticerView *) m_pActiveStickerView;

        CGPoint currentPoint = [touch locationInView:__view];
        UIGraphicsBeginImageContext(__view.frame.size);
        [__view.imageView.image drawInRect:__view.bounds];
        CGContextClearRect(UIGraphicsGetCurrentContext(), CGRectMake(currentPoint.x -15, currentPoint.y - 15, 20, 20));
        UIGraphicsEndImageContext();
        
    }
    
}

- (void) activiedView:(UIView *)view {
    m_pActiveStickerView = (TPSticerView *) view;
    [viewForTool setHidden:NO];
    is_hori = YES;
}

- (void)stickerCount
{
    sticker_count--;
    if (sticker_count == 0) {
        viewForTool.hidden = YES;
    }
}

@end
