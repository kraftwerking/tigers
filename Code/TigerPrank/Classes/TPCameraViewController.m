//
//  TPCameraViewController.m
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPCameraViewController.h"
#import "AVCamCaptureManager.h"
#import "AVCamRecorder.h"
#import "TPShareViewController.h"
#import "TPStickersViewController.h"
#import "TPSticerView.h"
#import "TPGlobal.h"
#import "CMPopTipView.h"
#define TOPTAG  100
#define BOTTOMTAG   200
@interface TPCameraViewController ()<AVCamCaptureManagerDelegate,TPStickersViewControllerDelegate, TPStickerViewDelegate,CMPopTipViewDelegate>
{
    BOOL is_erase;
    UIView *workingView;
    TPSticerView *localSticker;
    TPSticerView * m_pActiveStickerView;
    NSInteger sticker_tag;
}


@end

@implementation TPCameraViewController

- (void)showAds
{
#ifdef FREE_VERSION
    CGPoint origin = CGPointMake(0.0f,0.0f);
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait origin:origin];
    
    // Specify the ad unit ID.
    NSString *admobID;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        admobID = @"a153c86b0820e99";
        
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        admobID = @"a153c86b3090ecc";
    }
    bannerView_.adUnitID = admobID;//;
    
    
    bannerView_.rootViewController = self;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    //    if (![ud objectForKey:PURCHASED]) {
    //
    //        [self.view addSubview:bannerView_];
    //    }
    
    [self.view addSubview:bannerView_];
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    
    [ALInterstitialAd showOver:[[UIApplication sharedApplication] keyWindow]];
    
    
    //[[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
    [Chartboost showInterstitial:CBLocationHomeScreen];
    
#endif
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initCamera];
    
    sticker_tag = 1000;
    
    is_erase = NO;
    sticker_count = 0;
    
    viewForTool.hidden = YES;
    
    [self showAds];
    
    CGRect frameRect;
    
    CGFloat offsetY = 0;
    UIImage * skinImage;
    
//#ifdef FREE_VERSION
    offsetY = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? 48 : 80;
//#endif

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        frameRect = CGRectMake(320 - (60 + 15), 10+ (1024.0f / 480.0f)+offsetY, 60, 30);
        skinImage = [UIImage imageNamed:@"CameraPosition.png"];

    } else {
        frameRect = CGRectMake(768 - (120 + 15 * (768.0f / 320.0f)), 10 * (1024.0f / 480.0f)+offsetY, 120, 60);
        skinImage = [UIImage imageNamed:@"CameraPosition-ipad.png"];

    }
    cameraPosition_button = [[UIButton alloc] initWithFrame:frameRect];
    
    [cameraPosition_button setImage:skinImage forState:UIControlStateNormal];

    [cameraPosition_button addTarget:self action:@selector(onCameraPosition:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cameraPosition_button];
    [self.view bringSubviewToFront:cameraPosition_button];

 //   [cameraPosition_button release];

    // Do any additional setup after loading the view.
}
- (void)onCameraPosition:(id)sender {
    [self.captureManager toggleCamera];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   
    [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(showCB:) userInfo:nil repeats:YES];

}

- (void)showCB:(NSTimer *)timer
{
    //[[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
    [Chartboost showInterstitial:CBLocationHomeScreen];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initCamera
{
    CGRect frame = [self.cameraView frame];
    
    frame.size.height = self.view.frame.size.height;
    
    [self.cameraView setFrame:frame];
    if ([self captureManager] == nil)
    {
        AVCamCaptureManager *manager = [[AVCamCaptureManager alloc] init];
        [self setCaptureManager:manager];
        [[self captureManager] setDelegate:self];
        
        if ([[self captureManager] setupSession])
        {
            AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[[self captureManager] session]];
            UIView *view = self.cameraView;
            CALayer *viewLayer = [view layer];
            [viewLayer setMasksToBounds:YES];
            
            CGRect bounds = [view bounds];
            
            NSLog(@"%f  %f",bounds.size.width, bounds.size.height);
            [newCaptureVideoPreviewLayer setFrame:bounds];
            
            [newCaptureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
            [viewLayer insertSublayer:newCaptureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
            
            
            [self setCaptureVideoPreviewLayer:newCaptureVideoPreviewLayer];
            
        }
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[[self captureManager] session] startRunning];
    });

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)capturePhoto
{
    [[self captureManager] captureStillImage];
    
    UIView *flashView = [[UIView alloc] initWithFrame:[self.cameraView frame]];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    [[[self view] window] addSubview:flashView];
    
    [UIView animateWithDuration:.4f animations:^{
        [flashView setAlpha:0.f];
    }
                     completion:^(BOOL finished) {
                         [flashView removeFromSuperview];
                     }];
}

- (void)showStickerView
{
    TPStickersViewController *tpStickerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StickerVC"];
    tpStickerVC.delegate = self;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:tpStickerVC];
    
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)editImage:(UIImage *)_img
{
    
    NSLog(@"%f",self.view.frame.size.width);
    
    NSLog(@"%f , %f",_img.size.width,_img.size.height);
    
    [TPSticerView setActiveStickerView:nil];
//    
//    CGAffineTransform transform = CGAffineTransformMakeRotation(-75);
//    
//    for(UIView *view in _cameraView.subviews){
//        static int i = 0;
//        if ([view isKindOfClass:[TPSticerView class]]) {
//            NSLog(@"%f,%f",view.center.x,view.center.y);
//            view.transform = transform;
//
//
//        }
//    }
    
    if (([self captureManager].orientation == AVCaptureVideoOrientationLandscapeRight) || ([self captureManager].orientation == AVCaptureVideoOrientationLandscapeLeft)) {
        
        
        CGAffineTransform transform = CGAffineTransformMakeRotation(4.0 * M_PI / 2.0);
        
        for(UIView *view in _cameraView.subviews){
            
            if ([view isKindOfClass:[TPSticerView class]]) {
                NSLog(@"%f,%f",view.center.x,view.center.y);
                view.center = CGPointMake(view.center.y, self.view.frame.size.width - view.center.x); //CGRectMake(view.frame.origin.y, view.frame.origin.x+22, view.frame.size.width, view.frame.size.height);//(view.center.y,view.center.y);
                view.transform = transform;
            }
        }
        
        CGRect rect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
        
        _cameraView.frame = rect;
        
        
        UIImage *image = _img;
        UIGraphicsBeginImageContext(image.size);
        [image drawAtPoint:CGPointZero];
        
        CGFloat scale = image.size.width / _cameraView.frame.size.width;
        
        CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
        //   CGContextRotateCTM(UIGraphicsGetCurrentContext(), -90.0);
        
        [_cameraView.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        if ([[UIScreen mainScreen] bounds].size.height != 568) {
            
        }
        
        [self goShare:(UIImage *)tmp];
        
        
        transform = CGAffineTransformMakeRotation(1.0 * M_PI / 2.0);
        
        for(UIView *view in _cameraView.subviews){
            
            if ([view isKindOfClass:[TPSticerView class]]) {
                
                NSLog(@"%f,%f",view.center.x,view.center.y);
                
                view.center = CGPointMake(self.view.frame.size.width - view.center.y, view.center.x);//view.frame = CGRectMake(view.frame.origin.y, view.frame.origin.x, view.frame.size.width, view.frame.size.height);//(view.center.y,view.center.y);
                view.transform = transform;
            }
        }
        
        _cameraView.frame = [UIScreen mainScreen].bounds;


    }
    else{
        
        UIImage *image = _img;
        UIGraphicsBeginImageContext(image.size);
        [image drawAtPoint:CGPointZero];
        
        CGFloat scale = image.size.width / _cameraView.frame.size.width;
        
        CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
        //   CGContextRotateCTM(UIGraphicsGetCurrentContext(), -90.0);
        
        
        
        [_cameraView.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        if ([[UIScreen mainScreen] bounds].size.height != 568) {
            
        }
        
        [self goShare:(UIImage *)tmp];
    }

}

- (void)goShare:(UIImage *)_tmp
{
    TPShareViewController *tpShareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareVC"];
    
    tpShareVC.imageForShare = _tmp;
    tpShareVC.fromCamera = YES;
    [self.navigationController pushViewController:tpShareVC animated:YES];

}
#pragma mark
#pragma mark AVCamCapture Manager Delegate Methods
- (void)captureManager:(AVCamCaptureManager *)captureManager didFailWithError:(NSError *)error
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^{
        [[[UIAlertView alloc] initWithTitle:[error localizedDescription] message:[error localizedFailureReason] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title") otherButtonTitles:nil] show];
    });

}

- (void)captureManagerRecordingBegan:(AVCamCaptureManager *)captureManager
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void){
        
    });

}

- (void)captureManagerRecordingFinished:(AVCamCaptureManager *)captureManager fileURL:(NSURL *)_url assetURL:(NSURL *)_assetURL
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void){
        
    });

}

- (void)captureManagerStillImageCaptured:(AVCamCaptureManager *)captureManager image:(UIImage *)_img
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void){
        
        [self performSelector:@selector(editImage:) withObject:_img afterDelay:1.0f];
        
    });

}

- (IBAction)toolBarActions:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    switch (button.tag) {
        case TOPTAG:
        {
            //            TPSticerView *_view = (TPSticerView *)[imageViewForEdit viewWithTag:1000];
            if (m_pActiveStickerView != nil) {
                if (m_pActiveStickerView.m_bIsEditing == NO)
                    [m_pActiveStickerView doChangetoEditingState];
            }
            //            _view.is_enable = NO;
            //            is_erase = YES;
        }
            break;
            
        case TOPTAG + 1:
        {
            
            [m_pActiveStickerView removeActiveSticker];
            sticker_count--;
            if (sticker_count == 0) {
                viewForTool.hidden = YES;
            }
            
        }
            break;
            
        case TOPTAG + 2:
        {
            UIImage * image;
            image = m_pActiveStickerView.imageView.image;
            
            [TPSticerView setConvertedImage:m_pActiveStickerView image:[TPGlobal flipImageVertically:image]];
            image = nil;
            
            if (m_pActiveStickerView.m_bIsEditing == YES)
                [m_pActiveStickerView doChangetoEditingState];
            
        }
            break;
            
        case TOPTAG + 3:
        {
            UIImage * image;
            image = m_pActiveStickerView.imageView.image;
            
            [TPSticerView setConvertedImage:m_pActiveStickerView image:[TPGlobal flipImageHorizontally:image]];
            image = nil;
            
            if (m_pActiveStickerView.m_bIsEditing == YES)
                [m_pActiveStickerView doChangetoEditingState];
            
        }
            break;
        case TOPTAG + 4:
        {
            [m_pActiveStickerView changeActiveSticker];
        }
            break;
        case TOPTAG + 5:
        {
            [m_pActiveStickerView changeActiveSticker];
        }
            break;
        case TOPTAG + 6:
        {
            [TPSticerView setActiveStickerView:nil];
//            [[[UIAlertView alloc] initWithTitle:nil message:@"- TO SET IMAGE AS PROFILE PIC:\n - Save Image to Camera Roll \n - Tinder requires Facebook for most Tinder profile information including Profile pic. You will need to export desired profile picture to Facebook and then choose your Tinder Profile pic." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        }
            break;
        case TOPTAG + 7:
        {
//            CMPopTipView *popTipView  = [[CMPopTipView alloc] initWithMessage:@"TO SET IMAGE AS PROFILE PIC:\n Save Image to Camera Roll \n Tinder requires Facebook for most Tinder profile information including Profile pic. You will need to export desired profile picture to Facebook and then choose your Tinder Profile pic."];
//            popTipView.textColor = [UIColor whiteColor];
//            popTipView.delegate = self;
//            popTipView.backgroundColor = [UIColor redColor];
//            popTipView.animation = arc4random() % 2;
//            [popTipView presentPointingAtView:button inView:self.view animated:YES];
            
        }
            break;
        default:
            if (m_pActiveStickerView.m_bIsEditing == YES)
                [m_pActiveStickerView doChangetoEditingState];
            break;
    }
    
}

- (IBAction)bottomBarActions:(id)sender {
   
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case BOTTOMTAG:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case BOTTOMTAG + 1:
        {
            [self capturePhoto];
        }
            break;
        case BOTTOMTAG + 2:
        {
            [self showStickerView];
        }
            break;
        default:
            break;
    }
}

#pragma mark TPStickersViewControllerDelegate method

- (void)stickerDidCancel:(TPStickersViewController *)stickerViewController
{
    [stickerViewController dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)imageForSticker:(TPStickersViewController *)stickerViewController didFinished:(UIImage *)stickerImage
{
       [stickerViewController dismissViewControllerAnimated:YES completion:^{
        
        TPSticerView *view = [[TPSticerView alloc] initWithImage:stickerImage];
        view.is_enable = YES;
        CGFloat ratio = MIN((0.5 * _cameraView.frame.size.width) / view.frame.size.width, (0.5 * _cameraView.frame.size.height) / view.frame.size.height);
        
        [view setScale:ratio];
        view.center = CGPointMake(_cameraView.frame.size.width / 2, _cameraView.frame.size.height / 2);
        [_cameraView addSubview:view];
        
        m_pActiveStickerView = view;
        view.delegate = self;
        [TPSticerView setActiveStickerView:view];
        sticker_count ++;
        
        NSLog(@"%d",view.tag);
        view.alpha = 0.2;
        [UIView animateWithDuration:0.3f animations:^{
            view.alpha = 1.0f;
        }];
       }];

}



- (void) activiedView:(UIView *)view {
    m_pActiveStickerView = (TPSticerView *) view;
    [viewForTool setHidden:NO];
    is_hori = YES;
}

- (void)stickerCount
{
    sticker_count--;
    if (sticker_count == 0) {
        viewForTool.hidden = YES;
    }
}

@end
