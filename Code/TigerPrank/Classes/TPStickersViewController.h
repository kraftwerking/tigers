//
//  TPStickersViewController.h
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TPStickersViewController;

@protocol TPStickersViewControllerDelegate <NSObject>

- (void)imageForSticker:(TPStickersViewController *)stickerViewController didFinished:(UIImage *)stickerImage;
- (void)stickerDidCancel:(TPStickersViewController *)stickerViewController;

@end

@interface TPStickersViewController : UIViewController
{
    
    
    IBOutlet UICollectionView *myCollectionView;
    
}
@property (nonatomic, assign) id<TPStickersViewControllerDelegate> delegate;
@end
