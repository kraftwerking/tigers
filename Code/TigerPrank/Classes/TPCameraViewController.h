//
//  TPCameraViewController.h
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GADBannerView.h"

@class AVCamCaptureManager, AVCaptureVideoPreviewLayer;

@interface TPCameraViewController : UIViewController
{
    IBOutlet UIView *viewForTool;
    UIButton *      cameraPosition_button;      //

    NSInteger sticker_count;
    BOOL is_hori;
    GADBannerView *bannerView_;

}

@property (strong, nonatomic)AVCamCaptureManager *captureManager;
@property (strong, nonatomic)AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;

@property (strong, nonatomic) IBOutlet UIView *cameraView;

- (IBAction)toolBarActions:(id)sender;

- (IBAction)bottomBarActions:(id)sender;



@end
