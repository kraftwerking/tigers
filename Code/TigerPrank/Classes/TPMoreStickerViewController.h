//
//  TPMoreStickerViewController.h
//  TigerPrank
//
//  Created by ellisa on 7/17/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyIAPHelper.h"

@class TPMoreStickerViewController;

@protocol TPMoreStickerViewControllerDelegate <NSObject>

- (void)purchasedForSticker:(TPMoreStickerViewController *)stickerViewController didFinished:(BOOL)finished;
- (void)purchaseDidCancel:(TPMoreStickerViewController *)stickerViewController;

@end

@interface TPMoreStickerViewController : UIViewController
{
    
    IBOutlet UICollectionView *myCollectionView;
    
    BOOL isPurchased;
}
@property (nonatomic, assign) id<TPMoreStickerViewControllerDelegate> delegate;

- (IBAction)actions:(id)sender;



@end
