//
//  TPGlobal.m
//  TigerPrank
//
//  Created by ellisa on 7/13/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPGlobal.h"

@implementation TPGlobal

+ (UIImage *)flipImageHorizontally:(UIImage *)originalImage
{
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:originalImage];
    
    
    UIGraphicsBeginImageContext(tempImageView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGAffineTransform flipVertical = CGAffineTransformMake(-1.0, 0.0, 0.0, 1.0, tempImageView.frame.size.width, 0.0);

    
    CGContextConcatCTM(context, flipVertical);
    
    [tempImageView.layer renderInContext:context];
    
    UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return flipedImage;
}


+ (UIImage *)flipImageVertically:(UIImage *)originalImage
{
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:originalImage];
    
    
    UIGraphicsBeginImageContext(tempImageView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGAffineTransform flipVertical = CGAffineTransformMake(1,0,0,-1,0,tempImageView.frame.size.height);
    
    CGContextConcatCTM(context, flipVertical);
    
    [tempImageView.layer renderInContext:context];
    
    UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return flipedImage;
}

+ (BOOL)isPurchased {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"Purchased"];
}

+ (void)setPurchased {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Purchased"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
