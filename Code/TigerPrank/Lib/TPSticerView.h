//
//  TPSticerView.h
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPCircleView.h"

@protocol TPStickerViewDelegate <NSObject>
@optional;
- (void) activiedView : (UIView *) view;

@end

@interface TPSticerView : UIView
{
    UIImageView *_imageView;
    UIButton *_deleteButton;
    TPCircleView *_circleView;
    
    CGFloat _scale;
    CGFloat _arg;
    
    CGPoint _initialPoint;
    CGFloat _initialArg;
    CGFloat _initialScale;
}

- (UIImageView *)imageView;
- (id)initWithImage:(UIImage *)image;
- (void)setScale:(CGFloat)scale;
- (void) doChangetoEditingState;
- (void)removeActiveSticker;
- (void)changeActiveSticker;
+ (void)setActiveStickerView:(TPSticerView *)view;

+ (void)setConvertedImage:(TPSticerView *)view image:_convertedImg;
@property (nonatomic) BOOL is_enable;
@property (nonatomic) BOOL m_bIsEditing;

@property (nonatomic) id <TPStickerViewDelegate> delegate;

@end
