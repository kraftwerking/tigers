//
//  TPCircleView.m
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPCircleView.h"

@implementation TPCircleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.color = [UIColor blackColor];
        self.radius = 1;
        self.borderColor = [UIColor clearColor];
        self.borderWidth = 0;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef  context = UIGraphicsGetCurrentContext();
    
    CGRect rct = self.bounds;
    rct.origin.x = 0.5f * (rct.size.width - self.radius * rct.size.width);
    rct.origin.y = 0.5f * (rct.size.height - self.radius * rct.size.height);
    rct.size.width = self.radius * rct.size.width;
    rct.size.height = self.radius * rct.size.height;
    
    CGContextSetFillColorWithColor(context, self.color.CGColor);
    CGContextFillEllipseInRect(context, rct);
    
    
    CGContextSetStrokeColorWithColor(context, self.borderColor.CGColor);
    CGContextSetLineWidth(context, self.borderWidth);
    CGContextStrokeEllipseInRect(context, rct);
}

- (void)setColor:(UIColor *)color
{
    if (color != _color) {
        _color = color;
        [self setNeedsDisplay];
    }
}

- (void)setBorderColor:(UIColor *)borderColor
{
    if (borderColor != _borderColor) {
        _borderColor = borderColor;
        [self setNeedsDisplay];
    }
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    if (borderWidth != _borderWidth) {
        _borderWidth = borderWidth;
        [self setNeedsDisplay];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
