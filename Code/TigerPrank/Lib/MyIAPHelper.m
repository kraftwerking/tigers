//
//  FruitIAPHelper.m
//  BuyFruit
//
//  Created by Michael Beyer on 16.09.13.
//  Copyright (c) 2013 Michael Beyer. All rights reserved.
//

#import "MyIAPHelper.h"

#ifdef FREE_VERSION
static NSString *kIdentifierTiger       = @"com.wanakamobile.tigerprankfree.tiger";
#else
static NSString *kIdentifierTiger    = @"com.wanakamobile.tigerprankpro.tiger";



#endif

@implementation MyIAPHelper

// Obj-C Singleton pattern
+ (MyIAPHelper *)sharedInstance {
    static MyIAPHelper *sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        NSSet *productIdentifiers = [NSSet setWithObjects:
                                     kIdentifierTiger,
                                     nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

- (NSString *)imageNameForProduct:(SKProduct *)product
{
    if ([product.productIdentifier isEqualToString:kIdentifierTiger]) {
        return @"image_apple";
    }
       return nil;
}

- (NSString *)descriptionForProduct:(SKProduct *)product
{
    
       return nil;
}

@end
