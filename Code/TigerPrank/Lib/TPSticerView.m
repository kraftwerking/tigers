//
//  TPSticerView.m
//  TigerPrank
//
//  Created by ellisa on 7/8/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPSticerView.h"

@interface TPSticerView () {
    UITapGestureRecognizer * m_pTapGesture;
    UIPanGestureRecognizer * m_pPanGesture;
    
    BOOL m_bIsEnableEditing;
}

@end


@implementation TPSticerView

@synthesize m_bIsEditing;

+ (void)setConvertedImage:(TPSticerView *)view image:(id)_convertedImg
{
//    static TPSticerView *activeView = nil;
//    if(view != activeView){
//        [activeView setAvtive:NO];
//        activeView = view;
//        [activeView setAvtive:YES];
    if (view)
        [view.imageView setImage:(UIImage *)_convertedImg];
        
//        activeView.tag = 1000;
//        [activeView.superview bringSubviewToFront:activeView];
//    }
}

+ (void)setActiveStickerView:(TPSticerView *)view
{
    static TPSticerView *activeView = nil;
    if(view != activeView){
        [activeView setAvtive:NO];
        activeView = view;
        [activeView setAvtive:YES];
        
        activeView.tag = 1000;
        
        [activeView.superview bringSubviewToFront:activeView];
        [activeView.delegate activiedView:activeView];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (id)initWithImage:(UIImage *)image
{
    self = [super initWithFrame:CGRectMake(0, 0, image.size.width + 32, image.size.height + 32)];
    if (self) {
        _imageView = [[UIImageView alloc] initWithImage:image];
        _imageView.layer.borderColor = [[UIColor blackColor] CGColor];
        _imageView.layer.cornerRadius = 3;
        _imageView.center = self.center;
        [self addSubview:_imageView];
        
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setImage:[UIImage imageNamed:@"btn_delete"] forState:UIControlStateNormal];
        _deleteButton.frame = CGRectMake(0, 0, 32, 32);
        _deleteButton.center = _imageView.frame.origin;
        [_deleteButton addTarget:self action:@selector(pushedDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteButton];
        
        _circleView = [[TPCircleView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        _circleView.center = CGPointMake(_imageView.frame.size.width + _imageView.frame.origin.x, _imageView.frame.size.height + _imageView.frame.origin.y);
        _circleView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        _circleView.radius = 0.7;
        _circleView.color = [UIColor whiteColor];
        _circleView.borderColor = [UIColor blackColor];
        _circleView.borderWidth = 5;
        [self addSubview:_circleView];
        
        _scale = 1;
        _arg = 0;
        
        m_bIsEditing = NO;
        
        [self initGestures];
    }
    return self;
}

- (void)initGestures
{
    _imageView.userInteractionEnabled = YES;
    
    m_pTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)];
    m_pPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)];
    
    [_imageView addGestureRecognizer:m_pTapGesture];
    [_imageView addGestureRecognizer:m_pPanGesture];
    
    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (_is_enable) {
        UIView* view= [super hitTest:point withEvent:event];
        if(view==self){
            return nil;
        }
        return view;


    }
    return nil;
}

- (UIImageView*)imageView
{
    return _imageView;
}

- (void)pushedDeleteBtn:(id)sender
{
        TPSticerView *nextTarget = nil;
        
        const NSInteger index = [self.superview.subviews indexOfObject:self];
        
        for(NSInteger i=index+1; i<self.superview.subviews.count; ++i){
            UIView *view = [self.superview.subviews objectAtIndex:i];
            if([view isKindOfClass:[TPSticerView class]]){
                nextTarget = (TPSticerView*)view;
                break;
            }
        }
        
        if(nextTarget==nil){
            for(NSInteger i=index-1; i>=0; --i){
                UIView *view = [self.superview.subviews objectAtIndex:i];
                if([view isKindOfClass:[TPSticerView class]]){
                    nextTarget = (TPSticerView*)view;
                    break;
                }
            }
        }
        
        [[self class] setActiveStickerView:nextTarget];
        [self removeFromSuperview];
    
    if ([self.delegate respondsToSelector:@selector(stickerCount)]) {
        [self.delegate performSelector:@selector(stickerCount) withObject:nil];
    }
}

- (void)removeActiveSticker
{
    TPSticerView *nextTarget = nil;
    
    const NSInteger index = [self.superview.subviews indexOfObject:self];
    
    for(NSInteger i=index+1; i<self.superview.subviews.count; ++i){
        UIView *view = [self.superview.subviews objectAtIndex:i];
        if([view isKindOfClass:[TPSticerView class]]){
            nextTarget = (TPSticerView*)view;
            break;
        }
    }
    
    if(nextTarget==nil){
        for(NSInteger i=index-1; i>=0; --i){
            UIView *view = [self.superview.subviews objectAtIndex:i];
            if([view isKindOfClass:[TPSticerView class]]){
                nextTarget = (TPSticerView*)view;
                break;
            }
        }
    }
    
    [[self class] setActiveStickerView:nextTarget];
    [self removeFromSuperview];

}

- (void)changeActiveSticker
{
    TPSticerView *nextTarget = nil;
    
    const NSInteger index = [self.superview.subviews indexOfObject:self];
    
    for(NSInteger i=index+1; i<self.superview.subviews.count; ++i){
        UIView *view = [self.superview.subviews objectAtIndex:i];
        if([view isKindOfClass:[TPSticerView class]]){
            nextTarget = (TPSticerView*)view;
            break;
        }
    }
    
    if(nextTarget==nil){
        for(NSInteger i=index-1; i>=0; --i){
            UIView *view = [self.superview.subviews objectAtIndex:i];
            if([view isKindOfClass:[TPSticerView class]]){
                nextTarget = (TPSticerView*)view;
                break;
            }
        }
    }
    
    [[self class] setActiveStickerView:nextTarget];
}

- (void)setAvtive:(BOOL)active
{
    _deleteButton.hidden = !active;
    _circleView.hidden = !active;
    _imageView.layer.borderWidth = (active) ? 1/_scale : 0;
}

- (void)setScale:(CGFloat)scale
{
    _scale = scale;
    
    self.transform = CGAffineTransformIdentity;
    
    _imageView.transform = CGAffineTransformMakeScale(_scale, _scale);
    
    CGRect rct = self.frame;
    rct.origin.x += (rct.size.width - (_imageView.frame.size.width + 32)) / 2;
    rct.origin.y += (rct.size.height - (_imageView.frame.size.height + 32)) / 2;
    rct.size.width  = _imageView.frame.size.width + 32;
    rct.size.height = _imageView.frame.size.height + 32;
    self.frame = rct;
    
    _imageView.center = CGPointMake(rct.size.width/2, rct.size.height/2);
    
    self.transform = CGAffineTransformMakeRotation(_arg);
    
    _imageView.layer.borderWidth = 1/_scale;
    _imageView.layer.cornerRadius = 3/_scale;
}

- (void)viewDidTap:(UITapGestureRecognizer*)sender
{
    if (_is_enable) {
        [[self class] setActiveStickerView:self];

    }
}

- (void)viewDidPan:(UIPanGestureRecognizer*)sender
{
    if (_is_enable) {
        [[self class] setActiveStickerView:self];
        
        CGPoint p = [sender translationInView:self.superview];
        
        if(sender.state == UIGestureRecognizerStateBegan){
            _initialPoint = self.center;
        }
        self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);

    }
}

- (void)circleViewDidPan:(UIPanGestureRecognizer*)sender
{
    if (_is_enable) {
        CGPoint p = [sender translationInView:self.superview];
        
        static CGFloat tmpR = 1;
        static CGFloat tmpA = 0;
        if(sender.state == UIGestureRecognizerStateBegan){
            _initialPoint = [self.superview convertPoint:_circleView.center fromView:_circleView.superview];
            
            CGPoint p = CGPointMake(_initialPoint.x - self.center.x, _initialPoint.y - self.center.y);
            tmpR = sqrt(p.x*p.x + p.y*p.y);
            tmpA = atan2(p.y, p.x);
            
            _initialArg = _arg;
            _initialScale = _scale;
        }
        
        p = CGPointMake(_initialPoint.x + p.x - self.center.x, _initialPoint.y + p.y - self.center.y);
        CGFloat R = sqrt(p.x*p.x + p.y*p.y);
        CGFloat arg = atan2(p.y, p.x);
        
        _arg   = _initialArg + arg - tmpA;
        [self setScale:MAX(_initialScale * R / tmpR, 0.2)];

    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) doChangetoEditingState {
    
    if (m_bIsEditing == NO) {
        m_bIsEditing = YES;
        [_imageView removeGestureRecognizer:m_pTapGesture];
        [_imageView removeGestureRecognizer:m_pPanGesture];
    }
    else {
        m_bIsEditing = NO;
        [_imageView addGestureRecognizer:m_pTapGesture];
        [_imageView addGestureRecognizer:m_pPanGesture];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch * touch = [touches anyObject];
    if ([touch view] == self.imageView) {
        m_bIsEnableEditing = YES;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if (m_bIsEditing && m_bIsEnableEditing) {
        
        CGPoint currentPoint = [touch locationInView:self];
        
        CGRect frame = self.imageView.frame;
        frame.origin.x = 0.0;
        frame.origin.y = 0.0;
        
        UIGraphicsBeginImageContext(frame.size);
        [self.imageView.image drawInRect:frame];
        CGContextClearRect(UIGraphicsGetCurrentContext(), CGRectMake(currentPoint.x - 1, currentPoint.y - 1, 2, 2));
        self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    m_bIsEnableEditing = NO;
}

@end
