//
//  TPViewController.m
//  TigerPrank
//
//  Created by ellisa on 7/7/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPViewController.h"
#import "TPCameraViewController.h"
#import "VPImageCropperViewController.h"

#import "PECropViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

#define ORIGINAL_MAX_WIDTH 640.0f
@interface TPViewController ()<VPImageCropperDelegate,PECropViewControllerDelegate>

@end

@implementation TPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = NO;

	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([UIScreen mainScreen].bounds.size.height == 480) {
        imageViewForPimp.frame = CGRectMake(29, 214, 262, 58);
        imageViewForTitle.frame = CGRectMake(80, 187, 160, 26);
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
#ifdef FREE_VERSION
    [self showAds];
#endif

//    [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(showCB:) userInfo:nil repeats:YES];
    
}

- (void)showAds
{
    CGPoint origin = CGPointMake(0.0f,0.0f);
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait origin:origin];
    
    // Specify the ad unit ID.
    NSString *admobID;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        admobID = @"a153c86b0820e99";
        
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        admobID = @"a153c86b3090ecc";
    }
    bannerView_.adUnitID = admobID;//;
    
    
    bannerView_.rootViewController = self;
    //    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    //    if (![ud objectForKey:PURCHASED]) {
    //
    //        [self.view addSubview:bannerView_];
    //    }
    
    [self.view addSubview:bannerView_];
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    //AppLovin
    
    [ALInterstitialAd showOver:[[UIApplication sharedApplication] keyWindow]];
    //[[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
    [Chartboost showInterstitial:CBLocationHomeScreen];
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)openEditor:(UIImage *)_image
{

    PECropViewController *cropVC = [[PECropViewController alloc] init];
    
    cropVC.delegate = self;
    cropVC.image = _image;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cropVC];
    
    
    
    [self.navigationController presentViewController:nav animated:YES completion:nil];
    
    
//    ************************
    
    
//    UIImage *image = [self imageByScalingToMaxSize:_image];
//    //
//    VPImageCropperViewController *imgEditorVC = [[VPImageCropperViewController alloc] initWithImage:image cropFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
//    imgEditorVC.delegate = self;
//    
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:imgEditorVC];
//    
//    [self.navigationController presentViewController:nav animated:YES completion:^{
//        // TO DO
//    }];


}
#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    
//    [cropperViewController dismissViewControllerAnimated:YES completion:^{
//        // TO DO
//    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
//    [cropperViewController dismissViewControllerAnimated:YES completion:^{
//    }];
}

#pragma mark Actions

- (IBAction)cameraAction:(id)sender {
    
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//        
//        _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//        [self presentViewController:_picker animated:YES completion:nil];
//    }
    
}

- (IBAction)showAlbumAction:(id)sender {
    
    _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_picker animated:YES completion:nil];
}

- (IBAction)connectWithFB:(id)sender {
}

#pragma mark

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:selectedImage];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];

}

////Scale
//#pragma mark image scale utility
//- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
//    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
//    CGFloat btWidth = 0.0f;
//    CGFloat btHeight = 0.0f;
//    if (sourceImage.size.width > sourceImage.size.height) {
//        btHeight = ORIGINAL_MAX_WIDTH;
//        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
//    } else {
//        btWidth = ORIGINAL_MAX_WIDTH;
//        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
//    }
//    CGSize targetSize = CGSizeMake(btWidth, btHeight);
//    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
//}
//
//- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
//    UIImage *newImage = nil;
//    CGSize imageSize = sourceImage.size;
//    CGFloat width = imageSize.width;
//    CGFloat height = imageSize.height;
//    CGFloat targetWidth = targetSize.width;
//    CGFloat targetHeight = targetSize.height;
//    CGFloat scaleFactor = 0.0;
//    CGFloat scaledWidth = targetWidth;
//    CGFloat scaledHeight = targetHeight;
//    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
//    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
//    {
//        CGFloat widthFactor = targetWidth / width;
//        CGFloat heightFactor = targetHeight / height;
//        
//        if (widthFactor > heightFactor)
//            scaleFactor = widthFactor; // scale to fit height
//        else
//            scaleFactor = heightFactor; // scale to fit width
//        scaledWidth  = width * scaleFactor;
//        scaledHeight = height * scaleFactor;
//        
//        // center the image
//        if (widthFactor > heightFactor)
//        {
//            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
//        }
//        else
//            if (widthFactor < heightFactor)
//            {
//                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
//            }
//    }
//    UIGraphicsBeginImageContext(targetSize); // this will crop
//    CGRect thumbnailRect = CGRectZero;
//    thumbnailRect.origin = thumbnailPoint;
//    thumbnailRect.size.width  = scaledWidth;
//    thumbnailRect.size.height = scaledHeight;
//    
//    [sourceImage drawInRect:thumbnailRect];
//    
//    newImage = UIGraphicsGetImageFromCurrentImageContext();
//    if(newImage == nil) NSLog(@"could not scale image");
//    
//    //pop the context to get back to the default
//    UIGraphicsEndImageContext();
//    return newImage;
//}

@end
