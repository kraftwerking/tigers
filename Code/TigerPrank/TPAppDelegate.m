//
//  TPAppDelegate.m
//  TigerPrank
//
//  Created by ellisa on 7/7/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPAppDelegate.h"
#import <AVFoundation/AVAudioPlayer.h>
#import <AudioToolbox/AudioServices.h>

#import "TPGlobal.h"
#import "ALSdk.h"
#import <Chartboost/Chartboost.h>
#import <VungleSDK/VungleSDK.h>
#import <XplodeSDK/XplodeSDK.h>
#import <FacebookSDK/FacebookSDK.h>





@interface TPAppDelegate() <ChartboostDelegate>

@end

@implementation TPAppDelegate

bool isStartUp = YES;
@synthesize window;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if([[UIDevice currentDevice].systemVersion floatValue]>=8.0){
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
        if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
        }
#endif
        
    }
    
#ifdef FreeApp
    //if (!AppSettings::getBoolValue(Buy_RemoveAds))
    //{
        
        
        NSString *appHandle = @"TigerPrankPhotoEditorFREEDrawStampTigersAnimal";
        NSString *appSecret = @"5a09d5d94fdfb099ab61b3c97829e187";
        
        NSString* appID = @"858989788";
    
//        NSString *appHandle = @"GemDotsandBoxesConnect2014FREE";
//        NSString *appSecret = @"f44c315b78e28e878ac488885295be57";
//    
//        NSString* appID = @"837168787";
    
    
        VungleSDK *sdk = [VungleSDK sharedSDK];
        // start vungle publisher library
        [sdk startWithAppId:appID];
        [sdk setLoggingEnabled:YES];
        
        // Optional: automatically send uncaught exceptions to Google Analytics.
        //[GAI sharedInstance].trackUncaughtExceptions = YES;
        
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
        //[GAI sharedInstance].dispatchInterval = 20;
        
        // Optional: set Logger to VERBOSE for debug information.
        //[[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        
        // Initialize tracker. Replace with your tracking ID.
        //[[GAI sharedInstance] trackerWithTrackingId:@"UA-57527220-3"];
        
        [Xplode initializeWithAppHandle:appHandle
                              appSecret:appSecret
                   andCompletionHandler:nil];
        [self buttonGridTapped];
   // }
#endif
    // Override point for customization after application launch.
    
//    splashImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 320, 480)];
//    splashImage.image = [UIImage imageNamed:@"Default.png"];
//    [window addSubview:splashImage];
//    [window bringSubviewToFront:splashImage];
// 
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:3];
//    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:window cache:YES];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationWillStartSelector:@selector(playSound:finished:context:)];
//    [UIView setAnimationDidStopSelector:@selector(startupAnimationDone:finished:context:)];
//    splashImage.alpha = 0.0;
//    [UIView commitAnimations];
//    [TPGlobal setPurchased];
    [ALSdk initializeSdk];
    
    [self playsound];
    
    sleep(2.5f);

    
    return YES;
}
- (void)buttonGridTapped
{
    if ([Xplode isInitialized])
        [self presentPromotionForBreakpoint:@"interstitial"];
    else
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(buttonGridTapped) userInfo:nil repeats:NO];
}

- (void)presentPromotionForBreakpoint:(NSString *)breakpoint
{
    [Xplode presentPromotionForBreakpoint:breakpoint
                    withCompletionHandler:^(BOOL isPromotionAvailable, NSError *error) {
                        if (isPromotionAvailable) {
                            NSLog(@"Breakpoint %@ did load.", breakpoint);
                        }
                        else {
                            if (isStartUp && [breakpoint isEqualToString:@"interstitial"]) {
                                [self showAdWithOptions];
                            }
                            NSLog(@"Breakpoint %@ returned no promotion!", breakpoint);
                        }
                    }
                        andDismissHandler:^{
                            if (isStartUp && [breakpoint isEqualToString:@"interstitial"]) {
                                [self showAdWithOptions];
                            }
                            NSLog(@"Breakpoint %@ closed.", breakpoint);
                        }];
}
-(void)showAdWithOptions
{
    if (![[VungleSDK sharedSDK] isCachedAdAvailable]) {
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(showAdWithOptions) userInfo:nil repeats:NO];
        
        if(isStartUp){
            isStartUp = NO;
#ifdef FreeApp
            
            [Chartboost showInterstitial:CBLocationHomeScreen];

            //if (!AppSettings::getBoolValue(Buy_RemoveAds))
            //{
                //[[SNAdsManager sharedManager] giveMeBootUpAd];
            //}
#endif
        }
        
    }
    else{
        // Grab instance of Vungle SDK
        NSError *error;
        VungleSDK* sdk = [VungleSDK sharedSDK];
        
        // Dict to set custom ad options
        NSDictionary* options = @{VunglePlayAdOptionKeyOrientations: @(UIInterfaceOrientationMaskPortrait),
                                  VunglePlayAdOptionKeyIncentivized: @(YES),
                                  VunglePlayAdOptionKeyUser: @"user",
                                  VunglePlayAdOptionKeyShowClose: @(NO),
                                  // Use this to keep track of metrics about your users
                                  VunglePlayAdOptionKeyExtraInfoDictionary: @{VunglePlayAdOptionKeyExtra1: @"21",
                                                                              VunglePlayAdOptionKeyExtra2: @"Female"}};
        
        // Pass in dict of options, play ad
        [sdk playAd:self.window.rootViewController withOptions:options error:&error];
        
        isStartUp = NO;
    }
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Logs 'install' and 'app activate' App Events.
    [FBAppEvents activateApp];
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Begin a user session. Must not be dependent on user actions or any prior network requests.
    // Must be called every time your app becomes active.
    //[Chartboost startWithAppId:@"53c867d0c26ee4647fc933f8" appSignature:@"d248f3b5479c263f760757d9e404f83a70029e72" delegate:self];
    [Chartboost startWithAppId:@"53c867d0c26ee4647fc933f8"
                  appSignature:@"d248f3b5479c263f760757d9e404f83a70029e72"
                      delegate:self];
    
    //test
    
//    [Chartboost startWithAppId:@"53098dd09ddc356c0edf0411" appSignature:@"dca1dfbac1a3ec5988325585ec534a1ab5f89e51" delegate:self];

    
    
    // Show an ad at location "CBLocationHomeScreen"
//    [[Chartboost sharedChartboost] showInterstitial:CBLocationHomeScreen];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


static SystemSoundID soundID;

- (void)playsound
{
    if (soundID == 0)
    {
        NSString *path = [NSString stringWithFormat:@"%@/sound.mp3",[[NSBundle mainBundle] resourcePath]];
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) filePath, &soundID);
    }
    
    AudioServicesPlaySystemSound(soundID);
}

- (void)playSound:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
//    NSURL *soundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Growling tiger" ofType:@"mp3"]];
//    
//    AVAudioPlayer *audioPlayer = [[AVAudioPlayer  alloc] initWithContentsOfURL:soundURL error:nil];
//    [audioPlayer play];
    
    if (soundID == 0)
    {
        NSString *path = [NSString stringWithFormat:@"%@/sound.mp3",[[NSBundle mainBundle] resourcePath]];
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) filePath, &soundID);
    }
    
    AudioServicesPlaySystemSound(soundID);
    
}

- (void)startupAnimationDone:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    
}
@end
