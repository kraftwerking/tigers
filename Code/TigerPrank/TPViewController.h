//
//  TPViewController.h
//  TigerPrank
//
//  Created by ellisa on 7/7/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
@interface TPViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    
    UIImagePickerController *_picker;
    GADBannerView *bannerView_;
    
    
    IBOutlet UIImageView *imageViewForPimp;
    
    IBOutlet UIImageView *imageViewForTitle;
    

}
- (IBAction)cameraAction:(id)sender;
- (IBAction)showAlbumAction:(id)sender;

- (IBAction)connectWithFB:(id)sender;
@end
